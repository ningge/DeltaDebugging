import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import tools.DeltaDebugging;
import tools.MySelector;
import tools.SelectorTools;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;


public class RunDD {

    private static Logger logger = Logger.getLogger(RunDD.class.getName());

    public static void main(String [] args) {

        String filename = "dd.properties";
        Properties properties = SelectorTools.loadPropertiesFromFile(filename, RunDD.class);

        //get switch point from file
        String trueFile = properties.getProperty("trueFileDir");
        String falseFile = properties.getProperty("falseFileDir");
        String mainClass = properties.getProperty("main.class");
        int n = Integer.parseInt(properties.getProperty("n"));
        String outDir = properties.getProperty("outDir");
        Map<Integer, String> truePoint = SelectorTools.getSwitchPointFromFile(trueFile);
        Map<Integer, String> falsePoint = SelectorTools.getSwitchPointFromFile(falseFile);

        DeltaDebugging dd = new DeltaDebugging(truePoint, falsePoint, n, outDir);


        String[] config_str = {
//                "+classpath=/Users/ningge/machines/vagrantRoot/jpf/DEJAVU/out/production/DEJAVU" +
//                ":/Users/ningge/Downloads/fiveProgram/Log4j1/realLib/jacontebe-1.0.jar:" +
//                        "/Users/ningge/Downloads/fiveProgram/Log4j1/realLib/coring-1.4.jar",
               "+classpath=/Users/ningge/Downloads/fiveProgram/Log4j1/realLib/jacontebe-1.0.jar:" +
                "/Users/ningge/Downloads/fiveProgram/Log4j1/realLib/coring-1.4.jar:"+
                        "/Users/ningge/Downloads/JaConTeBe/versions.alt/lib/realLib/commons-collections-2.1.jar:"+
                        "/Users/ningge/Downloads/Dbcp.jar",
                "+vm.scheduler.sharedness.class=gov.nasa.jpf.vm.GlobalSharednessPolicy",
//                "+vm.shared.sync_detection=false",
                mainClass
        };

        //create JPF instance from config
        Config config = JPF.createConfig(config_str);

        MySelector mySelector = new MySelector(config, dd);
        mySelector.setLevel(Level.SEVERE);

        mySelector.run();
    }


}
