import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import tools.ExecTracker;
import tools.SelectorTools;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class GetInitTrace {

    public static void main(String[] args) {

        String filename = "dd.properties";
        Properties properties = SelectorTools.loadPropertiesFromFile(filename, RunDD.class);

        String mainClass = properties.getProperty("main.class");

        String[] config_str = {
                "+classpath=/Users/ningge/Downloads/fiveProgram/Log4j1/realLib/jacontebe-1.0.jar:" +
                        "/Users/ningge/Downloads/fiveProgram/Log4j1/realLib/coring-1.4.jar:"+
                        "/Users/ningge/Downloads/JaConTeBe/versions.alt/lib/realLib/commons-collections-2.1.jar:"+
                        "/Users/ningge/Downloads/Dbcp.jar",

                "+vm.scheduler.sharedness.class=gov.nasa.jpf.vm.GlobalSharednessPolicy",
//                "+vm.shared.sync_detection=false",
                mainClass
        };

        Config config = JPF.createConfig(config_str);

        File f = null;
        File f2 = null;
        FileOutputStream fileOutputStream = null;
        FileOutputStream fileOutputStream2 = null;
        String choiceLog = "/Users/ningge/logs/jpf/jpf-choice.log";
        String instructionLog = "/Users/ningge/logs/jpf/jpf-instruction.log";

        try {
            f = new File(choiceLog);
            if (!f.exists()) {
                f.createNewFile();
            }
            fileOutputStream = new FileOutputStream(f);

            f2 = new File(instructionLog);
            if (!f2.exists()) {
                f2.createNewFile();
            }
            fileOutputStream2 = new FileOutputStream(f2);
        } catch (IOException e) {
            e.printStackTrace();
        }

        JPF jpf = new JPF(config);
        ExecTracker execTracker = new ExecTracker(config, fileOutputStream, fileOutputStream2);
        jpf.addVMListener(execTracker);
        jpf.addSearchListener(execTracker);
        jpf.run();

        try {

            if (fileOutputStream != null)
                fileOutputStream.close();

            if (fileOutputStream2 != null){
                fileOutputStream2.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

