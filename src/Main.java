import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import tools.SelectorTools;
import tools.TestSelector;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by ningge on 01/08/2017.
 */
public class Main {

    public static void main(String[] args) {
        String[] config_str = {
                "+classpath=/Users/ningge/machines/vagrantRoot/jpf/DEJAVU/out/production/DEJAVU",
//                "+vm.scheduler.sharedness.class=gov.nasa.jpf.vm.GlobalSharednessPolicy",
//                "+listener=test.MyTracker",
//                "+search.class= gov.nasa.jpf.search.RandomSearch",
//                "+choice.trace=/Users/ningge/logs/jpf/jpf.log",
                "examples.AlarmClock"
        };

        Config config = JPF.createConfig(config_str);


//        ExecTracker execTracker = new ExecTracker(config);
        JPF jpf = new JPF(config);
        String [] threads = {
        };

        String filename = "/Users/ningge/logs/jpf/ddm.log";

        threads = SelectorTools.readThreads(filename);



        String instructionLog = "/Users/ningge/logs/jpf/jpf-instruction.log";
        File f = null;
        FileOutputStream outputStream = null;

        try {


            f = new File(instructionLog);
            if (!f.exists()) {
                f.createNewFile();
            }
            outputStream = new FileOutputStream(f);
        } catch (IOException e) {
            e.printStackTrace();
        }



        TestSelector testSelector = new TestSelector(config, threads, outputStream);

        jpf.addVMListener(testSelector);
        jpf.addSearchListener(testSelector);
        jpf.run();

        try {

            if (outputStream != null)
                outputStream.close();


        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
