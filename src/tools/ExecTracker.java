/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The Java Pathfinder core (jpf-core) platform is licensed under the
 * Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package tools;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.annotation.JPFOption;
import gov.nasa.jpf.listener.ChoiceTracker;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.vm.*;

import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.*;

/**
 * Listener tool to monitor JPF execution. This class can be used as a drop-in replacement for JPF, which is called by
 * ExecTracker. ExecTracker is mostly a VMListener of 'instructionExecuted' and a SearchListener of 'stateAdvanced' and
 * 'statehBacktracked'
 *
 * NOTE - the ExecTracker is machine type agnostic
 */

public class ExecTracker extends ListenerAdapter {

    @JPFOption(type = "Boolean", key = "et.print_insn", defaultValue = "true", comment = "print executed bytecode instructions")
    boolean printInsn = true;

    @JPFOption(type = "Boolean", key = "et.print_src", defaultValue = "false", comment = "print source lines")
    boolean printSrc = false;

    @JPFOption(type = "Boolean", key = "et.print_mth", defaultValue = "false", comment = "print executed method names")
    boolean printMth = false;

    @JPFOption(type = "Boolean", key = "et.skip_init", defaultValue = "true", comment = "do not log execution before entering main()")
    boolean skipInit = false;

    boolean showShared = false;

    int choiceCount  = 0;
    int switchCount = 0;

    PrintWriter out;
    PrintWriter out2;


    String lastThreadName = null;
    Instruction lastInstruction = null;
    ThreadInfo lastThread = null;

    String[] preficTrace = {
//            "main",
//            "main",
//            "main",
//            "main",
//            "Thread-1",
//            "Thread-2",
//            "Thread-3",
    };

    Map<Integer,String> map = new HashMap<>();

    boolean skip;

    public ExecTracker (Config config) {
        /** @jpfoption et.print_insn : boolean - print executed bytecode instructions (default=true). */
        printInsn = config.getBoolean("et.print_insn", true);

        /** @jpfoption et.print_src : boolean - print source lines (default=false). */
        printSrc = config.getBoolean("et.print_src", false);

        /** @jpfoption et.print_mth : boolean - print executed method names (default=false). */
        printMth = config.getBoolean("et.print_mth", false);

        /** @jpfoption et.skip_init : boolean - do not log execution before entering main() (default=true). */
        skipInit = config.getBoolean("et.skip_init", true);

        showShared = config.getBoolean("et.show_shared", true);

        if (skipInit) {
            skip = true;
        }

        out = new PrintWriter(System.out, true);
    }

    public ExecTracker (Config config, OutputStream outputStream) {
        /** @jpfoption et.print_insn : boolean - print executed bytecode instructions (default=true). */
        printInsn = config.getBoolean("et.print_insn", true);

        /** @jpfoption et.print_src : boolean - print source lines (default=false). */
        printSrc = config.getBoolean("et.print_src", false);

        /** @jpfoption et.print_mth : boolean - print executed method names (default=false). */
        printMth = config.getBoolean("et.print_mth", false);

        /** @jpfoption et.skip_init : boolean - do not log execution before entering main() (default=true). */
        skipInit = config.getBoolean("et.skip_init", true);

        showShared = config.getBoolean("et.show_shared", true);

        if (skipInit) {
            skip = true;
        }

        out = new PrintWriter(outputStream, true);
    }

    public ExecTracker (Config config, OutputStream outputStream, OutputStream outputStream2) {
        /** @jpfoption et.print_insn : boolean - print executed bytecode instructions (default=true). */
        printInsn = config.getBoolean("et.print_insn", true);

        /** @jpfoption et.print_src : boolean - print source lines (default=false). */
        printSrc = config.getBoolean("et.print_src", false);

        /** @jpfoption et.print_mth : boolean - print executed method names (default=false). */
        printMth = config.getBoolean("et.print_mth", false);

        /** @jpfoption et.skip_init : boolean - do not log execution before entering main() (default=true). */
        skipInit = config.getBoolean("et.skip_init", true);

        showShared = config.getBoolean("et.show_shared", true);

        if (skipInit) {
            skip = true;
        }

        out = new PrintWriter(outputStream, true);

        out2 = new PrintWriter(outputStream2, true);
    }

    /******************************************* SearchListener interface *****/
    //--- the ones we are interested in
    @Override
    public void searchStarted(Search search) {
        out.println("----------------------------------- search started");
//        if (skipInit) {
//            ThreadInfo tiCurrent = ThreadInfo.getCurrentThread();
//            miMain = tiCurrent.getEntryMethod();
//
//            out.println("      [skipping static init instructions]");
//        }

        if (out2 == null) {
            out2 = new PrintWriter(System.out, true);
        }
    }

  //  @Override
//    public void stateAdvanced(Search search) {
//        int id = search.getStateId();
//
//        out.print("----------------------------------- [" +
//                search.getDepth() + "] forward: " + id);
//        if (search.isNewState()) {
//            out.print(" new");
//        } else {
//            out.print(" visited");
//        }
//
//        if (search.isEndState()) {
//            out.print(" end");
//        }
//
//        out.println();
//
//        lastLine = null; // in case we report by source line
//        lastMi = null;
//        linePrefix = null;
//    }

//    @Override
//    public void stateProcessed (Search search) {
//        int id = search.getStateId();
//        out.println("----------------------------------- [" +
//                search.getDepth() + "] done: " + id);
//    }
//
//    @Override
//    public void stateBacktracked(Search search) {
//        int id = search.getStateId();
//
//        lastLine = null;
//        lastMi = null;
//
//        out.println("----------------------------------- [" +
//                search.getDepth() + "] backtrack: " + id);
//    }

    @Override
    public void searchFinished(Search search) {
        out.println("----------------------------------- search finished");
//        System.out.println(choiceCount + " " + switchCount);
        Set set2 = map.entrySet();
        Iterator iterator2 = set2.iterator();
        String lastThread = null;
        int index = 0;
        while(iterator2.hasNext()) {
            Map.Entry mentry2 = (Map.Entry)iterator2.next();
//            System.out.print("Key is: "+mentry2.getKey() + " & Value is: ");
            if (lastThread == null) {
                lastThread = (String) mentry2.getValue();
            } else {
                if (!mentry2.getValue().equals(lastThread)) {
                    out2.println( index + " " + mentry2.getValue() );
                }
                lastThread = (String) mentry2.getValue();
            }

            index++;
        }
//        DeltaDebugging.printMap(this.map);
    }

    /******************************************* VMListener interface *********/




//    record choice maked
    @Override
    public void choiceGeneratorAdvanced (VM vm, ChoiceGenerator<?> currentCG) {

        if (currentCG instanceof ThreadChoiceGenerator){
            ChoiceGenerator old = currentCG.getPreviousChoiceGenerator();


            choiceCount++; //count the number of thread switch
//            out.println("//------------------------- start choice trace");


            out.println("active threads:  --------------");
            Object[] choiceGenerators = ((ThreadChoiceGenerator) currentCG).getAllChoices();
            int i = -1;
            int temp = 0;

            for (Object choiceGenerator : choiceGenerators) {
                String threadName = ((ThreadInfo)choiceGenerator).getName();
                if (choiceCount < preficTrace.length + 1) {
//                    System.out.println(threadName + " " + preficTrace[choiceCount - 1]);

                    if (threadName.equals(preficTrace[choiceCount - 1])) {
                        i = temp;
                    }
                }
                out.println("  " + ((ThreadInfo)choiceGenerator).getName());
                temp++;
            }
//            System.out.println("");

            out.println("active threads:  --------------");
//
            int count = currentCG.getTotalNumberOfChoices();

//            Instruction prev = currentCG.getInsn().getPrev();
//            if (prev != null) {
//
//                System.out.println("prev:" + currentCG.getInsn().getPrev().getFilePos());
//            }
//
//            System.out.println("next:" + currentCG.getInsn().getFilePos() + "\n");
//
//            out.println("total of choices:" + count);
            Random random = new Random();
            if (i == -1) i = random.nextInt(count);
            currentCG.select(i);

            ThreadInfo ti = (ThreadInfo)choiceGenerators[i];
            Instruction instruction = ti.getPC();
            Instruction nextInstruction = ti.getNextPC();

//            out.println("selection is :" + i + " next thread name is  " + ((ThreadInfo)choiceGenerators[i]).getName());
//            if (instruction != null)
//            out.println(" instruction is :" + instruction.getClass().getName() + " location:" + instruction.getSourceLocation());
//            if (nextInstruction != null)
//            out.println(" next instruction is :" + nextInstruction.getClass().getName() + " location:" + nextInstruction.getSourceLocation());
//            out.println("//------------------------- end choice trace");

            out.println("\tthe " + choiceCount +  " choice is [" +  ((ThreadInfo)choiceGenerators[i]).getName() + "]");
            out.println("\tinstruction: " + instruction.getFilePos());



//            vm.printCurrentStackTrace();
//            System.out.println("\n");
//            vm.printLiveThreadStatus(new PrintWriter(System.out, true));
//            vm.printChoiceGeneratorStack();
            out.println("\n");

            map.put(choiceCount,((ThreadInfo)choiceGenerators[i]).getName() );

        }


    }






    //    this function  record thread switch and related instruction information
  //  @Override
//    public void instructionExecuted(VM vm, ThreadInfo currentThread, Instruction nextInstruction, Instruction executedInstruction) {
//        super.instructionExecuted(vm, currentThread, nextInstruction, executedInstruction);
//
//
//        if (lastInstruction == null && lastThread == null) {
//            switchCount++;
//            lastInstruction = executedInstruction;
//            lastThread = currentThread;
//            out2.println("[" + switchCount +"] thread is :" + lastThreadName);
//            out2.println("instruction is :" + executedInstruction.getClass().getName());
//            out2.println("location is :" + executedInstruction.getSourceLocation());
//        } else {
//            if (!lastThread.getName().equals(currentThread.getName())) {
//                switchCount++;
//                out2.println("old thread is :" + lastThread.getName() );
//                out2.println("instruction to run is :" + lastThread.getPC().getSourceLocation());
//                out2.println("instruction is :" + lastInstruction.getClass().getName());
//                out2.println("location is :" + lastInstruction.getSourceLocation() + "\n");
//                lastInstruction = executedInstruction;
//                lastThreadName = currentThread.getName();
//                out2.println("[" + switchCount +"]  thread is : " + lastThreadName);
//                out2.println("instruction is :" + executedInstruction.getClass().getName());
//                out2.println("location is :" + executedInstruction.getSourceLocation());
//            }
//        }
//    }

}

