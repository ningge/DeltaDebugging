package tools;

import java.io.File;
import java.util.*;
import java.util.logging.Logger;

public class DeltaDebugging {

    //Logger of this class
    public static Logger logger = Logger.getLogger(DeltaDebugging.class.getName());

    //true trace of the program
    Map<Integer, String> trueTrace = null;

    //false trace of the program
    Map<Integer, String> falseTrace = null;


    //index and value array form trueTrace
    Integer [] trueIndex = null;
    String [] trueValues = null;

    //index and value array form falseTrace
    Integer [] falseIndex = null;
    String [] falseValues  =null;


    public static void main(String [] args) {
        //get switch point from file
        String trueFile = "/Users/ningge/logs/jpf/true";
        String falseFile = "/Users/ningge/logs/jpf/false";
        Map<Integer, String> truePoint = SelectorTools.getSwitchPointFromFile(trueFile);
        Map<Integer, String> falsePoint = SelectorTools.getSwitchPointFromFile(falseFile);

        DeltaDebugging dd = new DeltaDebugging(truePoint, falsePoint, 2, "/Users/ningge/logs/jpf/");


        while(dd.hasNext()) {
//            System.out.println(Arrays.toString(dd.getNewTrace().entrySet().toArray()));
            Map<Integer, String> result = dd.getNewTrace();
            Set<Integer> temp = new TreeSet<>(result.keySet());

            for (Integer i : temp) {
                System.out.println(i + " " + result.get(i));
            }
        }

    }




    //number of the new sequence
    int deltaI = 1;

    // this means different sequence will be broken up into n pieces
    int n = 0;

    int start = 0;
    int end = 0;
    int difference = 0;

    //program output directory
    String outDir = null;

    public DeltaDebugging(Map<Integer, String> trueTrace, Map<Integer, String> falseTrace, int n, String outDir ) {
        this.trueTrace = trueTrace;
        this.falseTrace = falseTrace;
        this.n = n;
        this.outDir = outDir;


        this.mapToArray(trueTrace, true);
        this.mapToArray(falseTrace, false);


        int i = 0;

        //it is impossible for true and false trace have the same record, so don't need to check boundary
        while(trueIndex[i]  == falseIndex[i] && trueValues[i].equals(falseValues[i])) {
            start++;
            i++;
        }

        if (trueIndex.length == falseIndex.length) {
            i = trueIndex.length - 1;

            while(trueIndex[i]  == falseIndex[i] && trueValues[i].equals(falseValues[i])) {
                end++;
                i--;
            }

        }

        difference= (int)Math.ceil((Math.max(trueIndex.length, falseIndex.length) * 1.0  - start - end )/ n);

        logger.info("difference is :" + difference);

        if (!SelectorTools.isExist(outDir, false)) {
            try {

                if(!SelectorTools.creatFile(outDir, false)) {
                    logger.severe("can not create output directory");
                    System.exit(-1);
                }
            } catch (Exception e) {
                logger.severe(e.getMessage());
                System.exit(-1);
            }
        }
    }

    /**
     * get new delta trace
     * @return
     */
    public Map<Integer, String> getNewTrace() {

        if (!this.hasNext()) {
            return null;
        }

        if (n < 2) {
            logger.severe(" the value of n is too small");
            System.exit(-1);
        }
        Map<Integer, String> result = new HashMap<>();

        int newStart = deltaI * difference - difference + start;


        int i = 0;

        while(i < newStart && i < falseIndex.length) {
            result.put(falseIndex[i], falseValues[i]);
            i++;
        }


        boolean test = i < trueIndex.length;

        if (!test) {
            logger.warning("order is wrong 1");
            deltaI++;
            return null;
        }

        if (i > 0 && trueIndex[i] <= falseIndex[i - 1]) {
            logger.warning("order is wrong 2");
            deltaI++;
            return null;
        }

        while(i < difference + newStart && test) {
            result.put(trueIndex[i], trueValues[i]);
            i++;
            test = i < trueIndex.length;
        }

//        this.printMap(result);

        if ((difference + newStart) < falseIndex.length && trueIndex[i - 1] >= falseIndex[difference + newStart] ) {
            deltaI++;
            logger.warning("order is wrong 3");
            return null;
        }

        i = difference + newStart;


        while(i < falseIndex.length) {
            result.put(falseIndex[i], falseValues[i]);
            i++;
        }

//        this.printMap(result);


        deltaI++;
        return result;

    }

    public boolean hasNext() {
        return deltaI <= n;
    }

    public int getDeltaI() {
        return deltaI;
    }

    public String getOutDir() {
        if(!(outDir.charAt(outDir.length() - 1) == '/')) {
            outDir = outDir + "/";
        }

        return outDir;
    }

    public int getN() {
        return n;
    }

    private void mapToArray(Map<Integer, String> trace, boolean toTrue) {
        if (toTrue) {
            this.trueIndex = new Integer[trace.size()];
            this.trueValues = new String[trace.size()];
            SortedSet<Integer> trueKeys = new TreeSet<>(trueTrace.keySet());
            int index = 0;
            for(Integer i : trueKeys) {
                trueIndex[index] = i;
                trueValues[index] = trace.get(i);
                index++;
            }

        } else {
            this.falseIndex = new Integer[trace.size()];
            this.falseValues = new String[trace.size()];

            SortedSet<Integer> falseKeys = new TreeSet<>(falseTrace.keySet());
            int index = 0;
            for(Integer i : falseKeys) {
                falseIndex[index] = i;
                falseValues[index] = trace.get(i);
                index++;
            }

        }
    }

     public static void printMap(Map map) {
        System.out.println(Arrays.toString( map.entrySet().toArray()));
    }
}
