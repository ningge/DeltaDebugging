package tools;

import java.io.*;
import java.util.*;

/**
 * this class provide tools to MySelector
 */
public class SelectorTools {

    /**
     * read switch point from file
     * @param fileName
     * @return
     * @throws IOException
     */
    public static Map<Integer, String> getSwitchPointFromFile(String fileName) {
        Map<Integer, String> resultList = new HashMap<>();

        FileReader fileReader = null;
        BufferedReader bufferedReader = null;

        try {
            fileReader = new FileReader(fileName);
            bufferedReader = new BufferedReader(fileReader);

            String line = null;

            while ((line = bufferedReader.readLine()) != null ) {
                String [] temp = line.split(" ");
                resultList.put(Integer.parseInt(temp[0]), temp[1]);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {

                if (fileReader != null) {
                    fileReader.close();
                }

                if (bufferedReader != null) {
                    bufferedReader.close();
                }

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        return resultList;

    }


    public static String [] readThreads(String filename) {
        List<String> result = new ArrayList<>();

        FileReader fileReader = null;
        BufferedReader bufferedReader = null;

        try {
            fileReader = new FileReader(filename);
            bufferedReader = new BufferedReader(fileReader);
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                result.add(line.trim());
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result.toArray(new String[0]);
    }


    /**
     * write program trace to file
     * @param trace
     * @param filename
     */
    public static void writeToFile(Map trace, String filename, Boolean result) {

        try {

            if (!SelectorTools.isExist(filename, true)) {
                SelectorTools.creatFile(filename, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        FileWriter fileWriter = null;
        BufferedWriter bufferedWriter = null;

        try{
           fileWriter = new FileWriter(filename);
           bufferedWriter = new BufferedWriter(fileWriter);

           Set<Integer> set = new TreeSet(trace.keySet());

           String lastThread = null;
           String threadname = null;
           for (Integer i : set) {
               threadname = (String)trace.get(i);
               if (lastThread == null) {
                   lastThread = threadname;
                   if (result == null) {
                       lastThread = threadname;
                       bufferedWriter.write(i + " " + trace.get(i));
                       bufferedWriter.newLine();
                   }
               } else {
                   if (!lastThread.equals(threadname)) {
                       bufferedWriter.write(i + " " + trace.get(i));
                       bufferedWriter.newLine();
                       lastThread = threadname;
                   }
               }
           }

           if(result != null) {
               bufferedWriter.write("result is => " + result);
           }


        } catch ( IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try{

                if (fileWriter != null) {
                    fileWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }




    }


    /**
     * check if file or directory exist
     * by the way, file and directory all can be treat as file in JAVA
     *
     * @param name file or directory name
     * @param isFile true is file name , false is directory file
     * @return
     */
     public static boolean isExist(String name, boolean isFile) {
        File file = new File(name);

        if (file.exists()) {
            if (isFile) {
                return file.isFile();
            }

            return file.isDirectory();
        }

        return false;
    }

    /**
     *  create new file or directory
     * @param name
     * @param isFile
     * @return if file is created successfully
     */
    public static boolean creatFile(String name, boolean isFile) throws Exception{
        File file = new File(name);

        if (file.exists()) {
            throw new Exception("the file " + name + " is exist, do not recreate it ");
        }


        File parentDirectory = new File(file.getParent());

        if (!parentDirectory.exists()) {
            parentDirectory.mkdirs();
        }

        if (isFile) {

            if (parentDirectory.exists()) {
                file.createNewFile();
                return true;
            } else {
                return false;
            }
        } else {
            if (parentDirectory.exists()) {
                file.mkdir();
                return true;
            } else {
                return false;
            }
        }


    }

    /**
     *
     * @param filename
     * @param cls the class called this funtion
     * @return
     */
    public static Properties loadPropertiesFromFile(String filename, Class cls) {
        Properties properties = new Properties();
        InputStream in = cls.getResourceAsStream("dd.properties");
        try {

            properties.load(in);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return  properties;
    }

}
