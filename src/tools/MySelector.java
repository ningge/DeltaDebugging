package tools;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.vm.*;

import java.io.PrintWriter;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by ningge on 03/08/2017.
 *
 * this class will be used to contoll program choose thread by switch proint provided
 * without provided switch point, the program will random switch thread to run
 *
 *
 *
 */
public class MySelector extends ListenerAdapter{

    // the number of choice, will calculate dynamically
    private int choiceCount = 0;

    //logger
    private static Logger logger = Logger.getLogger(MySelector.class.getName());

    //record program trace
    Map<Integer,String> map  = new HashMap<>();

    // default program switch point
    Map<Integer, String> switchPoint = null;

    // local parameter record thread name
    String lastThread = null;
    Config config = null;

    //the result of last run
    Boolean result = true;

    //delta debugging object to generate new trace
    private DeltaDebugging dd = null;

    /**
     * default constructor
     * it will use default trace to controll program running
     * @param config JPF config
     */
    public MySelector(Config config) {
        this.setLevel(Level.SEVERE);

    }

    /**
     * constructor and provide switch point to hint how to controll program running
     * @param config
     * @param switchPoint
     */
    public MySelector(Config config, Map switchPoint) {
        this.switchPoint =  switchPoint;
    }



    public MySelector(Config config, DeltaDebugging dd) {
        this.dd = dd;
        this.config = config;
    }


    public void run() {
        if(dd != null) {
            //clear influence of last run
            this.choiceCount = 0;
            this.map = new HashMap<>();
            this.switchPoint = dd.getNewTrace();
            //if switch is invalid, get next
            while(this.switchPoint == null && dd.hasNext()) {
                this.switchPoint = dd.getNewTrace();
            }
//
            if(this.switchPoint == null) {
                System.exit(-1);
            }

            this.result = true;
            //run jpf
            JPF jpf = new JPF(config);
            jpf.addSearchListener(this);
            jpf.addVMListener(this);
            jpf.run();
        }
    }

    /**
     * set logger level
     * @param level
     */
    public void setLevel (Level level) {
        logger.setLevel(level);
    }

    /**
     * set logger output file
     * @param fileHandler
     * @param useConsole whether still use console as output destination
     */
    public void setLoggerFile(FileHandler fileHandler, Boolean useConsole) throws Exception {

        if (fileHandler != null)  {
            logger.addHandler(fileHandler);
        } else {
            throw new Exception("Filehandler specified is invalid");
        }

        if (!useConsole) {
            logger.setUseParentHandlers(false);
        }

    }


    /**
     * this function will switch program relied to switch point provided
     * @param vm
     * @param currentCG
     */
    @Override
    public void choiceGeneratorAdvanced(VM vm, ChoiceGenerator<?> currentCG)  {
        super.choiceGeneratorAdvanced(vm, currentCG);
        if (currentCG instanceof ThreadChoiceGenerator) {

            if (lastThread == null && choiceCount == 0) {//first choice
                lastThread = currentCG.getThreadInfo().getName();
                logger.fine(lastThread);
                currentCG.select(0);
            } else {
                try{
                   logger.info(Arrays.asList(switchPoint).indexOf(choiceCount) + " " + choiceCount + lastThread);
                    if (switchPoint.get(choiceCount) != null) {
                        selectNewThread(currentCG);
                    } else {
                        selectSameThread(currentCG);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        //record program exexute path
        map.put(choiceCount, lastThread);

        // calculate choice count
        choiceCount++;
    }

    @Override
    public void searchFinished(Search search) {
        if(dd == null) {

            System.out.println("finish");
            Set set2 = map.entrySet();
            Iterator iterator2 = set2.iterator();
            while(iterator2.hasNext()) {
                Map.Entry mentry2 = (Map.Entry)iterator2.next();
                logger.info("Key is: "+mentry2.getKey() + " & Value is: ");
                logger.info("\"" + mentry2.getValue() + "\",");
            }
        } else {
            int n = dd.getN();
            int deltaI = dd.getDeltaI() - 1;
            String filename = dd.getOutDir() + "/";



            DeltaDebugging.printMap(this.map);
            SelectorTools.writeToFile(this.switchPoint, filename + "plan" + deltaI + "_" + n, null);
            SelectorTools.writeToFile(this.map, filename + "real" + deltaI + "_" + n, result);
            if(dd.hasNext()) {

                this.run();
            }
        }
    }

    /**
     * as all the examples we run will throw an exception when it encounter an "concurrency bug"
     * so we can easily set program is go wrong when an exception thrown
     * @param vm
     * @param currentThread
     * @param thrownException
     */
    @Override
    public void exceptionThrown(VM vm, ThreadInfo currentThread, ElementInfo thrownException) {
        super.exceptionThrown(vm, currentThread, thrownException);

        this.result = false;
    }

    /**
     *  select new thread from choiceGenerators
     * @param currentCG
     * @throws Exception
     */
    private void selectNewThread(ChoiceGenerator<?> currentCG) throws Exception {

        Object[] choiceGenerators = ((ThreadChoiceGenerator) currentCG).getAllChoices();
        int temp = 0;

        String newThreadName = switchPoint.get(choiceCount);

        // judge if new thread equal to old thread
        if (newThreadName.equals(lastThread)) {
            throw new Exception("new thread to be switched is same to last thread");
        }

        // find index of new thread
        for (Object choiceGenerator : choiceGenerators) {
            String threadName = ((ThreadInfo)choiceGenerator).getName();
            if (threadName.equals(newThreadName)) {
                break;
            }
            temp++;
        }

        int i = currentCG.getTotalNumberOfChoices();

        // can't find new thread from current choice gererator
        if (temp == i) {
            currentCG.select(0);
            lastThread = currentCG.getThreadInfo().getName();
            logger.fine(lastThread);
            return;
//            throw new Exception("can't switch to thread specified " + choiceCount + " " + newThreadName);
        }


        //select the new thread we want
        currentCG.select(temp);

        // output new thread name
        lastThread = newThreadName;
        logger.fine( lastThread );
    }


    /**
     * make sure next thread is the same thread as the old, if it can't switch to the old
     * select a random thread
     * @param currentCG choiceGeneraor
     * @throws Exception
     */
    private void selectSameThread(ChoiceGenerator<?> currentCG) throws Exception {
        Object[] choiceGenerators = ((ThreadChoiceGenerator) currentCG).getAllChoices();

        int temp = 0;

        //select same thread to run
        for (Object choiceGenerator : choiceGenerators) {
            String threadName = ((ThreadInfo)choiceGenerator).getName();

            if (threadName.equals(lastThread)) {
                currentCG.select(temp);
                logger.fine( threadName );
                break;
            }
            temp++;
        }

        // if can't find, switch to default thread
        if (temp == choiceGenerators.length) {
            currentCG.select(0);
            logger.fine( ((ThreadInfo)choiceGenerators[0]).getName() );
        }
    }
}
