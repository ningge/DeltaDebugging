package tools;

import gov.nasa.jpf.Config;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.search.Search;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.ThreadChoiceGenerator;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class TestSelector  extends ListenerAdapter{

    Map<Integer, String> trace = new HashMap<>();

    int choiceCount = 0;
    int positon = 0 ;
    String [] threads = null;
    PrintWriter out = null;

    public TestSelector(Config config, String[] threads,  OutputStream outputStream) {
        this.threads = threads;
        this.out = new PrintWriter(outputStream, true);


        if (this.out == null) {
            this.out = new PrintWriter(System.out, true);
        }

    }


    @Override
    public void choiceGeneratorAdvanced(VM vm, ChoiceGenerator<?> currentCG) {
        super.choiceGeneratorAdvanced(vm, currentCG);


        if (currentCG instanceof ThreadChoiceGenerator) {

            int total = currentCG.getTotalNumberOfChoices();

            if (total == 1) {
                trace.put(choiceCount, currentCG.getThreadInfo().getName());
            } else {
                String threadToSwitch = threads[positon++];

                Object [] threads = ((ThreadChoiceGenerator) currentCG).getAllChoices();

                int i = 0;

                for (Object thread : threads) {
                    String threadName = ((ThreadInfo)thread).getName();
                    if (threadName.equals(threadToSwitch)) {
                        break;
                    }
                    i++;
                }

                try {

                    if (i == total) {
                        throw new Exception("can't switch to " + positon + " " + threadToSwitch + " " + choiceCount);
                    } else {
                        currentCG.select(i);
                        trace.put(choiceCount, threadToSwitch);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }

        choiceCount++;
    }

    @Override
    public void searchFinished(Search search) {
        super.searchFinished(search);

        Set temp = trace.entrySet();

        Iterator iterable = temp.iterator();

        while (iterable.hasNext()) {
            Map.Entry e = (Map.Entry)iterable.next();

            out.println(e.getValue());
        }
    }
}
