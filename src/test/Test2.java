package test;

import test.Thread1;
import test.Thread2;

/**
 * Created by ningge on 08/08/2017.
 */
public class Test2 {

    public  static int x = 0;
    public static void main(String[] args) {


        Thread1 thread1 = new Thread1();
        Thread2 thread2 = new Thread2();


        thread1.start();
        thread2.start();

        try {

            thread1.join();
           thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
