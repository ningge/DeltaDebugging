package test;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * Created by ningge on 07/08/2017.
 */
public class DeltaDebugging {

    //enum for program output
    public static enum Result  {
            RIGHT, WRONG, UNRESOLVED
    }

    private final static Logger logger = Logger.getLogger(DeltaDebugging.class.getName());

    // program trace of one output correctly
    String[] trueTrace = null;
    // thread switch of trueTrace
    int[] trueSwitch = null;


    // program trace of one output wrongly
    String[] falseTrace = null;
    // thread switch of falseTrace
    int[] falseSwitch = null;


    //start position for binary search
    int start = 0;

    //end position for binary search
    int end = 0;

    //program result for last run
    Result lastResult = null;

    public DeltaDebugging(String[] trueTrace, String[] falseTrace) throws Exception{
        this.trueTrace = trueTrace;
        this.falseTrace = falseTrace;

        this.trueSwitch = parseTrace(trueTrace);
        this.falseSwitch = parseTrace(falseTrace);
        fillShort(this.trueSwitch, this.falseSwitch, Math.max(trueTrace.length, falseTrace.length));

        System.out.println(Arrays.toString(trueSwitch));
        System.out.println(Arrays.toString(falseSwitch));

        this.start = 0;
        this.end = trueSwitch.length;

        lastResult = Result.RIGHT;

        this.setLoggerLevel(Level.SEVERE);
    }

    public DeltaDebugging(String[] trueTrace, String[] falseTrace, String[] newTrace, int start, int end, Result lastResult) throws Exception {
        if (lastResult == Result.RIGHT) {
            this.falseTrace = falseTrace;
            this.trueTrace = newTrace;
            this.start = (start + end) /2;
            this.end = end;
        } else if (lastResult == Result.WRONG) {
            this.falseTrace = newTrace;
            this.trueTrace = trueTrace;
            this.start = start;
            this.end = (start + end) / 2;
        }


        this.trueSwitch = parseTrace(trueTrace);
        this.falseSwitch = parseTrace(falseTrace);
        fillShort(this.trueSwitch, this.falseSwitch, Math.max(trueTrace.length, falseTrace.length));

        this.setLoggerLevel(Level.SEVERE);
    }


    /**
     * return program trace for new run
     *
     * @return
     */
    public String[] getNewTrace() {
        int mid = (this.end + this.start ) / 2;
//         System.out.println(mid);
        List<String> resultTrace = new ArrayList();
        //add start trace to new trace
        if (start > 0) {
            int i = 0;
            logger.info("false start: " +  this.falseSwitch[start]);
            while (i < this.falseSwitch[start]) {
                resultTrace.add(this.falseTrace[i]);
            }
        }


        logger.log(Level.INFO, resultTrace.toString());

        //do delta
        if (this.falseSwitch[mid - 1] > this.trueSwitch[mid]) {
//        TODO complete  trace generate process
        } else {

            // add half of the false trace to new trace
            String [] temp = new String[this.falseSwitch[mid - 1] + 1];
            System.arraycopy(this.falseTrace, start, temp, 0, temp.length);
            resultTrace.addAll(Arrays.asList(temp));
            logger.info(temp.length + "");
            // add half of the true trace to new trace
            temp = new String[this.trueSwitch[end -1] - this.trueSwitch[mid]];
            System.arraycopy(this.trueTrace, trueSwitch[mid], temp, 0, temp.length);
            resultTrace.addAll(Arrays.asList(temp));
            logger.info(temp.length + "");
        }
        logger.info(resultTrace.toString());
        if (this.end < trueTrace.length) {
            int i = this.falseSwitch[this.end - 1] - 1;

            while ( i < falseTrace.length) {
                resultTrace.add(falseTrace[i]);
                i++;
            }

            logger.info("false end:" + this.falseSwitch[this.end -1] + "false trace length: "  + falseTrace.length);
        }

        logger.info(resultTrace.toString());
        return  resultTrace.toArray(new String[0]);
    }


    /**
     * get thread switch from a trace
     * @param trace thread trace
     * @return
     */
    private int[] parseTrace(String [] trace) throws  Exception{

        ArrayList<Integer> temp = new ArrayList<Integer>();
        String lastThreadName = null;
        if (trace != null) {
            for (int i = 0; i < trace.length; i++) {
                if (lastThreadName != null) {
                    if (!lastThreadName.equals(trace[i])) {
                        temp.add(i);
                    }
                }
                lastThreadName = trace[i];
            }

            return temp.stream().mapToInt(Integer::intValue).toArray();
        }

        throw new Exception("invalid argument trace for parseTrace");
    }

    private void fillShort(int [] trueSwitch, int [] falseSwitch, int end) {
        int difference = Math.abs(trueSwitch.length - falseSwitch.length);

        if (difference != 0) {
            int[] filler = new int[difference];
            Arrays.fill(filler, end);

            if (trueSwitch.length > falseSwitch.length) {
                this.falseSwitch = concatString(falseSwitch, filler);
            } else {
                this.trueSwitch = concatString(trueSwitch, filler);
            }
        }
    }

    //concat two int array to one array
    private int[] concatString(int[] a, int [] b) {
        int[] temp = new int[a.length + b.length];
        System.arraycopy(a, 0, temp,0 , a.length);
        System.arraycopy(b, 0, temp, a.length, b.length );
        return temp;
    }


    public boolean isTerminated() {
    // TODO add other terminate condition
        return ((this.end - this.start) <= 1);
    }

    public void setLoggerLevel(Level level) {
        logger.setLevel(level);
    }


}
