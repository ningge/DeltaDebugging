package test;

public class IntQueue {

    // The queue holds integers in the range
    // of [1..numberOfElements - 1]
    static final int numberOfElements = 100;
    // link[N] is N’s successor in the queue
    int link[] = new int[numberOfElements];
    int head; // First element of queue
    int tail; // Last element of queue

    // Constructor
    IntQueue() {
        head = 0;
        tail = 0;
        for (int i = 0; i < numberOfElements; i++) {
            link[i] = 0;
        }
    }



    // Enqueue ELEM.
    public void enqueue(int elem) {
        link[elem] = 0;

        if (head == 0)
            head = elem;
        else{
            synchronized (this) {
                link[tail] = elem;
            }
        }

        tail = elem;
    }

    // Return first element of queue.
    // No error checking.
    public int dequeue() {
        int elem = head;
        if (elem == tail)
        tail = 0;

        synchronized (this) {
            head = link[head];
        }

        return elem;
    }

    // Print elements of queue
    public void print() {
        for (int e = head; e != 0; e = link[e])
            System.out.print(e + " ");
        System.out.println();
    }
}
