package test;

import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

/**
 * Created by ningge on 03/08/2017.
 */
public class NewTracker extends ListenerAdapter {

//    public MyTracker

    @Override
    public void instructionExecuted(VM vm, ThreadInfo currentThread, Instruction nextInstruction, Instruction executedInstruction) {
        super.instructionExecuted(vm, currentThread, nextInstruction, executedInstruction);
        System.out.println(executedInstruction.getClass().getName());
        System.out.println(currentThread.getName());
    }
}
