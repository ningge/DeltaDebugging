## DEJAVU

Project to implement experiment in the paper of  [Choi, Jong-Deok, and Andreas Zeller. "Isolating failure-inducing thread schedules." ACM SIGSOFT Software Engineering Notes. Vol. 27. No. 4. ACM, 2002.](http://dl.acm.org/citation.cfm?id=566211)


### Dependencies
   - [JPF-V8][1] 
   - [JAVA8][2]
   - [SIR repository][3]
    
   [1]: http://javapathfinder.sourceforge.net
   [2]: http://www.oracle.com/technetwork/java/javase/overview/java8-2100321.html
   [3]: http://sir.unl.edu/portal/index.php
   
### Directory structure
```
    root --|
           |---src all source file here
                |
                |———examples/ example programs to be run use jpf
                |
                |----test/ some classes used to test funtion and example classes in zeller's paper 
                |
                |----tools/tools needed to record and replay program the program to be run
                      |---MySelector.java dispatch program by trace provieded
                      |---ExecTracker.java record program execute trace
                      |---SelectorTools.java tools used by **MySelector**
                |----result/ record steps result of examples by using dd algorithm
                |----Main.java usually uesed to see the output of example classes
                |----RunDD.java run Delta Debugging here
                |----RunTest.java random dispatch program to get true trace and false trace of one program
```

### Result Files Naming Rules
> this section describe how files and directories named

- for directories below `result/` : directory name is the program name 
- for directories below `result/{program}/` : directory name is `test{test number}`
- for files below result/{program}/{testX}/:
    1. falseTrace init false trace
    2. trueTrace init true trace
    3. final_result if we can get a result by using DD for whole program, result is here
- for directories below `result/{program}/{testX}/`: directory name is `round{round number}`
- for files below `result/{program}/{testX}/{roundY}/`
    1. `{planA_B}`: B is the number of deltas, the order of this plan trace among B 
    2. `{realA_B}`: the real execute path recorded directed by `{planA_B}`

